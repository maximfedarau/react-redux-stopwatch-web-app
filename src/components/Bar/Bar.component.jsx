import React from "react"

import {
    Button,
    Box,
    Center
  } from "@chakra-ui/react"

import store from "../../store/store"

import {
    Provider,
    connect
} from "react-redux"
import BarContent from "../BarContent/BarContent.component"

function changeShowBarVisiblity() {
    return {
        type: "CHANGE_BAR_VISIBILITY"
    }
}

function changeBarInputValues(barInputValues) {
    return {
        type: "CHANGE_BAR_INPUT_VALUES",
        barInputValues
    }
}

function setTime(time) {
    return {
        type: "SET_TIME",
        time
    }
}

function mapStateToProps(state) {
    return {
        showBar: state.barReducer.showBar,
        barInputValues: state.barReducer.barInputValues,
        time: state.timeReducer.time
    }
}

const mapDispatchToProps = {
    changeShowBarVisiblity,
    changeBarInputValues,
    setTime
}


function UnconnectedBarComponent(props) {

    function onClickShowBarHandler() {
        props.changeShowBarVisiblity()
    }

    const inputIds={
        seconds: 'seconds',
        minutes: 'minutes',
        hours: 'hours'
    }

    function onChangeHandler(event) {
        props.changeBarInputValues({
            ...props.barInputValues,[event.target.id]: event.target.value
        })
    }

    function onClickInputHandler() {
        const {barInputValues} = props
        const timeSum = (Number(barInputValues.seconds)+Number(barInputValues.minutes*60)+Number(barInputValues.hours*3600))
        if ((props.time+timeSum) >=0) props.setTime(timeSum)
        else props.setTime(-props.time)
        props.changeBarInputValues({
            seconds: '',
            minutes: '',
            hours: '',
        })
    }

    return (
        <>
            <Center marginTop="10px" marginRight="30px">
            <Button onClick={onClickShowBarHandler}>{(props.showBar) ? "Hide Bar" : "Show Bar"}</Button>
            {(props.showBar) && <BarContent
            inputValues={props.barInputValues} 
            inputIds={inputIds} 
            onChangeHandler={onChangeHandler}
            onClickInputHandler={onClickInputHandler}/>}
            </Center>
        </>
    )
}

const ConnectedBarComponent = connect(mapStateToProps,mapDispatchToProps)(UnconnectedBarComponent)

export default function BarComponent() {

    return (
        <Provider store={store}>
            <ConnectedBarComponent/>
        </Provider>
    )
}