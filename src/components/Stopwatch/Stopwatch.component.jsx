import React from 'react'

import store from '../../store/store'

import {
    Provider,
    connect
} from "react-redux"
import { 
    Button,
    Center,
    Text
 } from '@chakra-ui/react'

function setTime(time) {
    return {
        type: "SET_TIME",
        time
    }
}

function changeStopwatchStatus(stopwatchStatus) {
    return {
        type: "CHANGE_STOPWATCH_STATUS",
        stopwatchStatus
    }
}

function mapStateToProps(state) {
    return {
        time: state.timeReducer.time,
        stopwatchStatus: state.timeReducer.stopwatchStatus
    }
}

const mapDispatchToProps = {
    setTime,
    changeStopwatchStatus
}

const UnconnectedStopwatch = (props) => {

    React.useEffect(() => {
        if (props.stopwatchStatus) {
            let interval = setInterval(() => {
                props.setTime(1)
              },1000)
              return function cleanup() {
              clearInterval(interval)
            }
        }
    },[props.stopwatchStatus])

    function onClickStopwatchStatusHandler() {
        props.changeStopwatchStatus(!props.stopwatchStatus)
    }

    function onClickResumeHandler() {
        props.setTime(-props.time)
        props.changeStopwatchStatus(false)
    }

    function formattedTime() {
        const pureSeconds = props.time%60
        const pureMinutes = parseInt(props.time/60)%60
        const pureHours = parseInt(props.time/3600)
        return `${parseInt(pureHours/10)}${parseInt(pureHours%10)}:${parseInt(pureMinutes/10)}${parseInt(pureMinutes%10)}:${parseInt(pureSeconds/10)}${parseInt(pureSeconds%10)}`
      }

    return (
        <div>
            <Text fontSize={50} textAlign="center">Stopwatch</Text>
            <Text fontSize={150}>{formattedTime()}</Text>
            <Center gap={10}>
            <Button onClick={onClickStopwatchStatusHandler}>{(props.stopwatchStatus) ? "Stop" : "Activate"}</Button>
            <Button onClick={onClickResumeHandler}>Resume</Button>
            </Center>
        </div>
    )
}

const ConnectedStopwatch = connect(mapStateToProps,mapDispatchToProps)(UnconnectedStopwatch)

export default function Stopwatch() {
    return (
        <Provider store={store}>
            <ConnectedStopwatch/>
        </Provider>
    )
}