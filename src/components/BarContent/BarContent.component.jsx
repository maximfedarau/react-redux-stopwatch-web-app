import React from 'react'

import {
    Input,
    Button
} from "@chakra-ui/react"

export default function  BarContent({inputValues,inputIds,onChangeHandler,onClickInputHandler}) {
    const {seconds, minutes, hours} = inputValues
    return (
        <div>
            <Input type="number" placeholder='Your seconds:' value={seconds} id={inputIds.seconds} onChange={onChangeHandler}/>
            <Input type="number" placeholder="Your minutes:" value={minutes} id={inputIds.minutes} onChange={onChangeHandler}/>
            <Input type="number" placeholder="Your hours:" value={hours}  id={inputIds.hours} onChange={onChangeHandler}/>
            <Button onClick={onClickInputHandler}>Apply</Button>
        </div>
    )
}