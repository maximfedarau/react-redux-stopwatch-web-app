import React from "react"

import './App.css';

import {
  Center,
  Box,
  ChakraProvider
} from "@chakra-ui/react"

import BarComponent from "./components/Bar/Bar.component";
import Stopwatch from "./components/Stopwatch/Stopwatch.component";

function App() {

  

  return (
    <ChakraProvider>
      <Center>
        <Box>
        <Stopwatch/>
        <BarComponent/>
        </Box>
      </Center>
    </ChakraProvider>
  );
}

export default App;
