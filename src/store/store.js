import {
    createStore,
    combineReducers
} from 'redux'


function timeReducer(state={time:0,stopwatchStatus:false},action) {
    switch(action.type) {
        case "SET_TIME":
            return {...state,time: state.time+action.time}
        case "CHANGE_STOPWATCH_STATUS":
            return {...state,stopwatchStatus: action.stopwatchStatus}
        default:
            return state
    }
}

function barReducer(state={showBar: false, barInputValues: {
    seconds: '',
    minutes: '',
    hours: '',
}},action) {
    switch (action.type) {
        case ("CHANGE_BAR_VISIBILITY"):
            return {...state, showBar: !state.showBar}
        case "CHANGE_BAR_INPUT_VALUES":
            return {...state,barInputValues: action.barInputValues}
        default:
            return state
    }
}

const reducer = combineReducers({barReducer,timeReducer})

const store = createStore(reducer)

export default store

